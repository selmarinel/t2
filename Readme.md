###TODO
Add exceptions handlers 
- for some I remember exception

Move event dispatcher from Application lvl to domain

Think about moving model to domain/// Think about it

Granulate Manager class to many use-case services

Think about infrastructure request-response functionality, move to domain lvl/// think about it 


On Windows:

   In root directory of project run 
    
    .\deploy\build\dev.sh


On Linux\Mac OS
    
   In root directore of project run
    
    sh deploy\build\dev.sh
---- 

Default host for project - `127.0.0.1:81`   

#### API Endpoints:
 
###### Create:
   Input format:JSON
     
   Output format: JSON
   
   Data: `{"name":"%classroom name%"}`  
  
   EntryPoint `[POST] /api/classrooms`
       
###### Update:
   Input format:JSON
   
   Output format: JSON
   
   Data: `{"name":"%classroom name%"}`  
  
   EntryPoint `[PUT] /api/classrooms/:id`
       
###### Delete:
   Input format:JSON
   
   Output format: JSON
  
   EntryPoint `[DELETE] /api/classrooms/:id`
       
###### Get:
   Output format: JSON
    
   EntryPoint `[GET] /api/classrooms/:id`
   
###### List:
   Output format: JSON
   
   Parameters: ?page=:page&take=:take&active=0|1
   
   EntryPoint `[GET] /api/classrooms`

###### Activate
   Input format: JSON
    
   Output format: JSON
   
   Data: `{activate: true/false}`
   
   EntryPoint `[PUT] /api/classrooms/:id/activate`
   
 
Second work:

1.A: https://gist.github.com/selmarinel/b8a4ae3eb159345c43321205c480d474

1.B: https://gist.github.com/selmarinel/d27dc723cbbe8e12410b27c0a1da0f8d

1.C: https://gist.github.com/selmarinel/7f0bcd9ef20cc392c9944599c45dc001

1.D: https://gist.github.com/selmarinel/1c8a99be3fca5c83a06e7dea329e6572

1.E: https://gist.github.com/selmarinel/6801632df6e85520fc6e3ccf2eb73ee5

2.A https://gist.github.com/selmarinel/c28efdf842df88996c5f06a3ffcc44b1

2.B https://gist.github.com/selmarinel/c8b837f43c54faf1d97ed402f192cd5f