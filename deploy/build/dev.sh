#!/usr/bin/env bash

COMPOSE_FILES="-f deploy/docker-compose.yaml"

NAME_PREFIX="classrooms"

docker-compose -p $NAME_PREFIX $COMPOSE_FILES rm -f
docker-compose -p $NAME_PREFIX $COMPOSE_FILES build --pull
docker-compose -p $NAME_PREFIX $COMPOSE_FILES up -d --force-recreate

echo Start composer features

docker exec classrooms_php_1 composer install
docker exec classrooms_php_1 composer setup

echo DB is ready. prepare to load dump data

echo Complete
sleep 10
