<?php

namespace App\Application\Controller\Classroom;

use App\Application\Service\JsonMapper;
use App\Domain\Service\ClassroomManagerInterface;
use App\Domain\ValueObject\Request\ClassroomVO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ActivateAction
 */
class ActivateAction
{
    /**
     * @Route(path="/api/classrooms/{id}/activate", methods={"PUT"}, name="classroom_activation")
     *
     * @ParamConverter(name="classroomVO", class=ClassroomVO::class, converter="request_converter", options={"groups":{"activate"}})
     *
     * @param ClassroomVO $classroomVO
     * @param ClassroomManagerInterface $classroomManager
     * @param JsonMapper $jsonMapper
     *
     * @return JsonResponse
     */
    public function __invoke(
        ClassroomVO $classroomVO,
        ClassroomManagerInterface $classroomManager,
        JsonMapper $jsonMapper
    ): JsonResponse
    {
        return $jsonMapper->returnJson($classroomManager->setActivation($classroomVO));
    }
}