<?php

namespace App\Application\Controller\Classroom;

use App\Application\Service\JsonMapper;
use App\Domain\Service\ClassroomManagerInterface;
use App\Domain\ValueObject\Request\ClassroomVO;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CreateAction
 */
class CreateAction
{
    /**
     * @Route(path="/api/classrooms", methods={"POST"}, name="create_classooom")
     *
     * @ParamConverter(name="classroomVO", converter="request_converter", class=ClassroomVO::class, options={"groups":{"create"}})
     *
     * @param ClassroomVO $classroomVO
     * @param ClassroomManagerInterface $classroomManager
     * @param JsonMapper $jsonMapper
     *
     * @return JsonResponse
     */
    public function _invoke(
        ClassroomVO $classroomVO,
        ClassroomManagerInterface $classroomManager,
        JsonMapper $jsonMapper
    ): JsonResponse
    {
        return $jsonMapper->returnJson($classroomManager->create($classroomVO), JsonResponse::HTTP_CREATED);
    }
}