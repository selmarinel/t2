<?php

namespace App\Application\Controller\Classroom;

use App\Application\Service\JsonMapper;
use App\Domain\Service\ClassroomManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GetAction
 */
class GetAction
{
    /**
     * @Route(path="/api/classrooms/{id}", name="get_classroom", methods={"GET"}, requirements={"id" = "\d+"})
     *
     * @param int $id
     * @param ClassroomManagerInterface $classroomManager
     * @param JsonMapper $jsonMapper
     * todo make mapper on events level
     *
     * @return JsonResponse
     */
    public function __invoke(int $id, ClassroomManagerInterface $classroomManager, JsonMapper $jsonMapper): JsonResponse
    {
        return $jsonMapper->returnJson($classroomManager->getOne($id));
    }
}