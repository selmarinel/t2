<?php

namespace App\Application\Controller\Classroom;

use App\Application\Service\JsonMapper;
use App\Domain\Service\ClassroomManagerInterface;
use App\Domain\ValueObject\Request\ClassroomFilterVO;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ListAction
 */
class ListAction
{
    /**
     * @Route(path="/api/classrooms", methods={"GET"}, name="classroom_list")
     *
     * @ParamConverter(name="filterVO", class=ClassroomFilterVO::class, converter="request_converter")
     *
     * @param ClassroomFilterVO $filterVO
     * @param ClassroomManagerInterface $classroomManager
     * @param JsonMapper $jsonMapper
     *
     * @return JsonResponse
     */
    public function __invoke(
        ClassroomFilterVO $filterVO,
        ClassroomManagerInterface $classroomManager,
        JsonMapper $jsonMapper
    ): JsonResponse
    {
        return $jsonMapper->returnJson($classroomManager->getFilteredList($filterVO));
    }
}