<?php

namespace App\Application\Controller\Classroom;

use App\Domain\Service\ClassroomManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RemoveAction
 */
class RemoveAction
{
    /**
     * @Route(path="/api/classrooms/{id}", methods={"DELETE"}, name="delete_classroom")
     *
     * @param int $id
     * @param ClassroomManagerInterface $classroomManager
     *
     * @return JsonResponse
     */
    public function __invoke(int $id, ClassroomManagerInterface $classroomManager)
    {
        $classroomManager->remove($id);

        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }
}