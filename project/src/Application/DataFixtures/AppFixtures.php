<?php

namespace App\Application\DataFixtures;

use App\Infrastructure\Model\Classroom;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $factory = Factory::create();

        $batchSize = 20;
        for ($i = 0; $i < 100; $i++) {
            $classRoom = new Classroom();
            $classRoom->setName($factory->name)
                ->setIsActive($factory->boolean(60));
            $manager->persist($classRoom);

            if (($i % $batchSize) === 0) {
                $manager->flush();
                $manager->clear(); // Detaches all objects from Doctrine!
            }
        }
        $manager->flush();
        $manager->clear();
    }
}
