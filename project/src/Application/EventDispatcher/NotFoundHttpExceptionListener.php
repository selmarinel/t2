<?php

namespace App\Application\EventDispatcher;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class NotFoundHttpException
 */
class NotFoundHttpExceptionListener implements EventSubscriberInterface
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        if ($event->getException() instanceof NotFoundHttpException) {
            $response = new JsonResponse([
                'message' => 'not.found',
                'data' => $event->getException()->getMessage()
            ], JsonResponse::HTTP_NOT_FOUND);
            $event->allowCustomResponseCode();
            $event->setResponse($response);
        }
    }

    /**
     * @return array|void
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }
}