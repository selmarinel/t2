<?php

namespace App\Application\EventDispatcher;

use App\Application\Exception\ValidationException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class ValidationListener
 */
class ValidationListener implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getException();

        if (!$exception instanceof ValidationException) {
            return;
        }
        $violationList = $exception->getViolationList();

        $violationExceptionData = [];

        for ($i = 0; $i < $violationList->count(); $i++) {
            $violation = $violationList->get($i);
            $violationExceptionData[] = [$violation->getPropertyPath() => $violation->getMessage()];
        }

        $response = new JsonResponse(
            [
                'message' => 'validation.errors',
                'errors' => $violationExceptionData
            ],
            JsonResponse::HTTP_FORBIDDEN
        );

        $event->allowCustomResponseCode();
        $event->setResponse($response);

    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException'
        ];
    }
}