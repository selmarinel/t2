<?php

namespace App\Application\Service;

use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class JsonMapper
 */
class JsonMapper
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param $data
     * @param int $code
     * @param array $headers
     *
     * @return JsonResponse
     */
    public function returnJson($data, int $code = JsonResponse::HTTP_OK, array $headers = []): JsonResponse
    {
        $data = $this->serializer->serialize($data, 'json');

        return new JsonResponse($data, $code, $headers, true);
    }
}