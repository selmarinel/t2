<?php

namespace App\Domain\Model\ClassRoom;

/**
 * Interface GetClassroomInterface
 */
interface GetClassroomInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime;

    /**
     * @return bool
     */
    public function isIsActive(): bool;
}