<?php

namespace App\Domain\Model\ClassRoom;

/**
 * Interface SetClassroomInterface
 */
interface SetClassroomInterface
{
    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self;

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setIsActive(bool $active): self;
}