<?php

namespace App\Domain\Repository;

use App\Domain\Model\ClassRoom\GetClassroomInterface;
use App\Domain\Model\ClassRoom\SetClassroomInterface;
use App\Domain\ValueObject\Request\ClassroomFilterVO;
use App\Domain\ValueObject\Request\ClassroomVO;

/**
 * Interface ClassroomRepositoryInterface
 */
interface ClassroomRepositoryInterface
{
    /**
     * @param ClassroomVO $classroomVO
     *
     * @return SetClassroomInterface
     */
    public function create(ClassroomVO $classroomVO): SetClassroomInterface;

    /**
     * @param int $id
     *
     * @return GetClassroomInterface|null
     */
    public function findById(int $id): ?GetClassroomInterface;

    /**
     * @param ClassroomFilterVO $filterVO
     * @return GetClassroomInterface[]
     */
    public function findByFilter(ClassroomFilterVO $filterVO): array;

    /**
     * @param ClassroomFilterVO $filterVO
     *
     * @return int
     */
    public function countByFilter(ClassroomFilterVO $filterVO): int;

    /**
     * @param SetClassroomInterface $classroom
     *
     * @return GetClassroomInterface
     */
    public function save(SetClassroomInterface $classroom): GetClassroomInterface;

    /**
     * @param GetClassroomInterface $classroom
     */
    public function delete(GetClassroomInterface $classroom): void;
}