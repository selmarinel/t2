<?php

namespace App\Domain\Service;

use App\Domain\Model\ClassRoom\GetClassroomInterface;
use App\Domain\ValueObject\Request\ClassroomFilterVO;
use App\Domain\ValueObject\Request\ClassroomVO;
use App\Domain\ValueObject\Request\ClassroomVO as ClassroomRequest;
use App\Domain\ValueObject\Response\ClassroomVO as ClassroomResponse;
use App\Domain\ValueObject\Response\ListVO;

interface ClassroomManagerInterface
{
    /**
     * @param ClassroomRequest $classroomVO
     *
     * @return GetClassroomInterface
     */
    public function create(ClassroomRequest $classroomVO): GetClassroomInterface;

    /**
     * @param ClassroomRequest $classroomVO
     *
     * @return GetClassroomInterface
     */
    public function update(ClassroomRequest $classroomVO): GetClassroomInterface;

    /**
     * @param int $id
     */
    public function remove(int $id): void;

    /**
     * @param int $id
     *
     * @return GetClassroomInterface|null
     */
    public function getOne(int $id): ?GetClassroomInterface;

    /**
     * @param ClassroomRequest $classroomVO
     *
     * @return GetClassroomInterface
     */
    public function setActivation(ClassroomVO $classroomVO): GetClassroomInterface;

    /**
     * @param ClassroomFilterVO $filterVO
     *
     * @return ListVO
     */
    public function getFilteredList(ClassroomFilterVO $filterVO): ListVO;
}