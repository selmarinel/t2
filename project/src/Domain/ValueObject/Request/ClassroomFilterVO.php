<?php

namespace App\Domain\ValueObject\Request;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ClassroomFilterVO
 */
class ClassroomFilterVO
{
    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    private $take = 10;

    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    private $page = 1;

    /**
     * @Serializer\SkipWhenEmpty()
     * @Serializer\Type("bool")
     *
     * @var bool|null
     */
    private $active;

    /**
     * @return int
     */
    public function getTake(): int
    {
        return $this->take;
    }

    /**
     * @param int $take
     *
     * @return self
     */
    public function setTake(int $take): self
    {
        $this->take = $take;

        return $this;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     *
     * @return self
     */
    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     *
     * @return self
     */
    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        $offset = ($this->getPage() - 1) * $this->getTake();

        if ($offset < 0) {
            return 0;
        }

        return $offset;
    }
}