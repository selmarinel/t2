<?php

namespace App\Domain\ValueObject\Request;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ClassroomVO
 */
class ClassroomVO
{
    /**
     * @var int
     *
     * @Assert\NotBlank(groups={"update", "activate"})
     * @Assert\Type("integer", groups={"update", "activate"})
     *
     * @Serializer\Type("integer")
     * @Serializer\Groups({"update", "activate"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"create", "update"})
     * @Assert\Length(
     *     min = 2,
     *     max = 100,
     *     groups={"update", "create"}
     * )
     *
     * @Serializer\Type("string")
     * @Serializer\Groups({"create", "update"})
     */
    private $name;

    /**
     * @Assert\Type("boolean", groups={"activate"})
     *
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"activate"})
     *
     * @var bool
     */
    private $activate;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActivate(): bool
    {
        return $this->activate;
    }

    /**
     * @param bool $activate
     *
     * @return self
     */
    public function setActivate(bool $activate): self
    {
        $this->activate = $activate;

        return $this;
    }
}