<?php

namespace App\Domain\ValueObject\Response;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ListVO
 */
class ListVO
{
    /**
     * @Serializer\Type("integer")
     *
     * @var int
     */
    private $total;

    /**
     *
     * @var array
     */
    private $items;

    public function __construct(int $total, array $items)
    {
        $this->total = $total;
        $this->items = $items;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @param int $total
     *
     * @return self
     */
    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     *
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->items = $items;

        return $this;
    }
}