<?php

namespace App\Infrastructure\Model;

use App\Domain\Model\ClassRoom\GetClassroomInterface;
use App\Domain\Model\ClassRoom\SetClassroomInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Infrastructure\Repository\ClassroomRepository;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Classroom
 * @ORM\Entity(repositoryClass=ClassroomRepository::class)
 */
class Classroom implements SetClassroomInterface, GetClassroomInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Serializer\Type("string")
     *
     * @Assert\Type(type="string")
     * @Assert\Length(
     *     min = 2,
     *     max = 100
     * )
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $created;

    /**
     * @ORM\Column(type="boolean")
     *
     * @Assert\Type("boolean")
     *
     * @var bool
     */
    private $isActive;

    /**
     * Classroom constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->isActive = true;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return SetClassroomInterface
     */
    public function setId(int $id): SetClassroomInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return SetClassroomInterface
     */
    public function setName(string $name): SetClassroomInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     *
     * @return SetClassroomInterface
     */
    public function setCreated(\DateTime $created): SetClassroomInterface
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIsActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return self
     */
    public function setIsActive(bool $isActive): SetClassroomInterface
    {
        $this->isActive = $isActive;

        return $this;
    }
}