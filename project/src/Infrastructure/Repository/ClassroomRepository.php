<?php

namespace App\Infrastructure\Repository;

use App\Domain\Model\ClassRoom\GetClassroomInterface;
use App\Domain\Model\ClassRoom\SetClassroomInterface;
use App\Domain\Repository\ClassroomRepositoryInterface;
use App\Domain\ValueObject\Request\ClassroomFilterVO;
use App\Domain\ValueObject\Request\ClassroomVO;
use App\Infrastructure\Model\Classroom;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ClassroomRepository
 * @property EntityManagerInterface $_em
 */
class ClassroomRepository extends ServiceEntityRepository implements ClassroomRepositoryInterface
{
    /**
     * ClassroomRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Classroom::class);
    }

    /**
     * @param int $id
     *
     * @return GetClassroomInterface|null
     */
    public function findById(int $id): ?GetClassroomInterface
    {
        /** @var Classroom $classroom */
        $classroom = $this->find($id);

        return $classroom;
    }

    /**
     * @param ClassroomFilterVO $filterVO
     * @return GetClassroomInterface[]
     */
    public function findByFilter(ClassroomFilterVO $filterVO): array
    {
        $builder = $this->createQueryBuilder('c')
            ->setMaxResults($filterVO->getTake())
            ->setFirstResult($filterVO->getOffset())
            ->orderBy('c.created');

        if (!is_null($filterVO->getActive())) {
            $builder->where('c.isActive = :active')
                ->setParameter('active', $filterVO->getActive());
        }

        return $builder
            ->getQuery()
            ->getResult();
    }

    /**
     * @param ClassroomFilterVO $filterVO
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter(ClassroomFilterVO $filterVO): int
    {
        $builder = $this->createQueryBuilder('c');
        $builder->select('count(c.id)');

        if (!is_null($filterVO->getActive())) {
            $builder->where('c.isActive = :active')
                ->setParameter('active', $filterVO->getActive());
        }

        return (int) $builder->getQuery()->getSingleScalarResult();
    }


    /**
     * @param SetClassroomInterface|Classroom $classroom
     *
     * @return GetClassroomInterface
     */
    public function save(SetClassroomInterface $classroom): GetClassroomInterface
    {
        $this->_em->persist($classroom);
        $this->_em->flush();

        return $classroom;
    }

    /**
     * @param ClassroomVO $classroomVO
     *
     * @return SetClassroomInterface
     *
     * @throws \Exception
     */
    public function create(ClassroomVO $classroomVO): SetClassroomInterface
    {
        $classroom = new Classroom();
        $classroom->setName($classroomVO->getName());

        return $classroom;
    }

    /**
     * @param GetClassroomInterface $classroom
     */
    public function delete(GetClassroomInterface $classroom): void
    {
        $this->_em->remove($classroom);
        $this->_em->flush();
    }
}