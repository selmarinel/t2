<?php

namespace App\Infrastructure\Service;

use App\Domain\Model\ClassRoom\GetClassroomInterface;
use App\Domain\Model\ClassRoom\SetClassroomInterface;

use App\Domain\Repository\ClassroomRepositoryInterface;
use App\Domain\Service\ClassroomManagerInterface;
use App\Domain\ValueObject\Request\ClassroomFilterVO;
use App\Domain\ValueObject\Request\ClassroomVO;
use App\Domain\ValueObject\Response\ListVO;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ClassroomManager
 * todo add mapper (response)
 */
class ClassroomManager implements ClassroomManagerInterface
{
    /**
     * @var ClassroomRepositoryInterface
     */
    private $classroomRepository;

    /**
     * ClassroomManager constructor.
     * @param ClassroomRepositoryInterface $classroomRepository
     */
    public function __construct(ClassroomRepositoryInterface $classroomRepository)
    {
        $this->classroomRepository = $classroomRepository;
    }

    /**
     * @param ClassroomVO $classroomVO
     *
     * @return GetClassroomInterface
     *
     * @throws \Exception
     */
    public function create(ClassroomVO $classroomVO): GetClassroomInterface
    {
        $classroom = $this->classroomRepository->create($classroomVO);

        //todo extra logic. For example set user in classroom from token storage

        return $this->classroomRepository->save($classroom);
    }

    /**
     * @param ClassroomVO $classroomVO
     *
     * @return GetClassroomInterface
     */
    public function update(ClassroomVO $classroomVO): GetClassroomInterface
    {
        /** @var SetClassroomInterface|GetClassroomInterface $classroom */
        if ($classroom = $this->classroomRepository->findById($classroomVO->getId())) {
            $classroom->setName($classroomVO->getName());

            return $this->classroomRepository->save($classroom);
        }

        throw new NotFoundHttpException(sprintf('Classroom %d not found', $classroomVO->getId()));
    }

    /**
     * @param int $id
     */
    public function remove(int $id): void
    {
        $classroom = $this->classroomRepository->findById($id);

        if (!$classroom) {
            //todo throws another message
            throw new NotFoundHttpException(sprintf('Classroom %d not found', $id));
        }

        $this->classroomRepository->delete($classroom);
    }

    /**
     * @param int $id
     *
     * @return GetClassroomInterface|null
     */
    public function getOne(int $id): ?GetClassroomInterface
    {
        if ($classroom = $this->classroomRepository->findById($id)) {
            return $classroom;
        }

        //todo throws another message
        throw new NotFoundHttpException(sprintf('Classroom %d not found', $id));
    }

    /**
     * @param ClassroomVO $classroomVO
     *
     * @return GetClassroomInterface
     */
    public function setActivation(ClassroomVO $classroomVO): GetClassroomInterface
    {
        //todo refact add to repository method who return set interface
        /** @var SetClassroomInterface|GetClassroomInterface $classroom */
        if ($classroom = $this->classroomRepository->findById($classroomVO->getId())) {
            $classroom->setIsActive($classroomVO->isActivate());
            return $this->classroomRepository->save($classroom);
        }

        //todo throws another message
        throw new NotFoundHttpException(sprintf('Classroom %d not found', $classroomVO->getId()));
    }

    /**
     * @param ClassroomFilterVO $filterVO
     *
     * @return ListVO
     */
    public function getFilteredList(ClassroomFilterVO $filterVO): ListVO
    {
        return new ListVO(
            $this->classroomRepository->countByFilter($filterVO),
            $this->classroomRepository->findByFilter($filterVO)
        );
    }
}